"use strict";

var gulp = require('gulp');
var connect = require('gulp-connect');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');

gulp.task('connect', function() {
    connect.server({
        root: './',
        port: 8888,
        livereload: true
    });
});

gulp.task('styles', function() {
    gulp.src('sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./dist/css'))
        .pipe(connect.reload());
});

gulp.task('scripts', function() {
    return gulp.src(['js/*.js', 'js/service-list/*.js'])
        .pipe(concat('main.js'))
        .pipe(uglify({mangle: false}))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('html', function() {
    gulp.src('index.html')
        .pipe(connect.reload());
});

//Watch task

gulp.task('watch',function() {
    gulp.watch(['sass/*.scss', 'js/*.js', 'index.html'], ['styles', 'scripts', 'html']);
});

gulp.task('default', ['connect', 'styles', 'scripts', 'html', 'watch']);