angular.
module('denteezApp').
component('serviceList', {
    templateUrl: 'js/service-list/services-list.template.html',
    controller: function DenteezAppController($http) {
        var self = this;
        self.orderProp = 'name';
        $http({
            method: 'GET',
            url: 'http://504080.com/api/v1/services/categories',
            headers: {'Authorization': token}
        }).then(function successCallback(response) {
            $('.loader').hide();
            self.services = response.data.data;
        }, function errorCallback(response) {
            $('.loader').hide();
            $('.error-status').text(response.status);
            $('#errorModal').modal('show');
            $('.not-found-wrap').show();
            $('.services-list').hide();
        });
    }
});
